''''
    Here we are setting CreateObject as CLASS-LEVEL fixture
    So Pytest will call this fixture method before calling any test case method
    from class. Fixture method will create Calculator class object and will set it
    to CLASS-LEVEL variable from which test case methods will use this object
    to call subtraction method
'''
import pytest

from Calculator import Calculator

@pytest.mark.usefixtures("CreateObject")
class TestSubtraction:

    Calc=None

    '''
             @pytest.fixture()
             def CreateObject(self):
                 TestSubtraction.Calc=Calculator()
    '''

    def testSubstractionWithPositiveValues(self,ProvidePositiveValuesForAddition):
        #Calc=Calculator()
        #Result=TestSubtraction.Calc.Subtraction(40,20)
        #assert Result==20,"Substraction does not work with Positive values"
        Result = TestSubtraction.Calc.Subtraction(ProvidePositiveValuesForAddition["Num1"], ProvidePositiveValuesForAddition["Num2"])
        assert Result == ProvidePositiveValuesForAddition["Result"], "Substraction does not work with Positive values"

    def testSubstractionWithOnePositiveOneNegativeValue(self):
        #Calc = Calculator()
        Result = TestSubtraction.Calc.Subtraction(60, -20)
        assert Result == 80, "Substraction does not work with 1 Positive value and 1 Negative value"

