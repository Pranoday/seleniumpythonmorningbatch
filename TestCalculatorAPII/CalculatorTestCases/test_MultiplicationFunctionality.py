''''
    If test cases are written in a class then its name should start with
    word "Test"

    When testcase steps for multiple testcases remain same and only test data
    changes then such test cases are called Data-driven testcases
'''
import pytest

from Calculator import Calculator

@pytest.mark.usefixtures("CreateObject")
class TestMultiplication:
    Calc=None
    '''
    @pytest.fixture()
    def CreateObj(self):
        TestMultiplication.Calc=Calculator()
    '''

    def testMultiplicationWithPositiveValues(self,ProvidePositiveValues):
        #Result=TestMultiplication.Calc.Multiplication(10,20)
        #assert 200==Result,"Multiplication does not work with positive values"
        Result = TestMultiplication.Calc.Multiplication(ProvidePositiveValues[0], ProvidePositiveValues[1])
        assert ProvidePositiveValues[2]==Result,"Multiplication does not work with positive values"
