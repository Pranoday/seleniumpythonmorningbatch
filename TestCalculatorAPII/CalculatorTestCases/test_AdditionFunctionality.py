'''
    PyTest is a Unit testing tool used in Python environment
    Pytest recognises those python files as a test case files names of which
    either starts with "test_" OR ends with "_test"

    So valid file names are for example: test_AdditionFunctionality OR
    AdditionFunctionality_test

    Testcase method name should start with "test" word

    Creating an object of Calculator class is required for each testcase method
    written in this file.So instead creating object in each testcase method
    we have created a separate function(Fixture) to create Calculator class object
    and mentioned its name for every testcase function.Pytest will call this
    Fixture function before calling testcase.
'''
import pytest

from Calculator import Calculator
Calc=None
@pytest.fixture()
def CreateObj():
    global  Calc
    Calc=Calculator()
def testAdditionWithPositiveValues(CreateObj):
    #Calc=Calculator()
    Result=Calc.Addition(40,50)
    '''
    if(Result==90):
        print("Addition works fine with positive values..PASSED")
    else:
        print("Addition does not work fine with positive values..PASSED")
    '''
    assert 90==Result,"Addition not working with Positive values"

def testAdditionWithPositiveAndNegativeValues(CreateObj):
    #Calc = Calculator()
    Result = Calc.Addition(80, -50)
    assert 30 == Result, "Addition not working with 1 Positive value and 1 Negative value"

def testAdditionWithBothZeroValues(CreateObj):
    #Calc = Calculator()
    Result = Calc.Addition(0, 0)
    assert 0 == Result, "Addition not working with both 0 values"

def testAdditionWithBothNegativeValues(CreateObj):
    #Calc = Calculator()
    Result = Calc.Addition(-10, -20)
    assert -30 == Result, "Addition not working with both both Negative values"
