''''
    This file contains fixture common across multiple testcase files/
    testcase classes

'''
import pytest

from Calculator import Calculator

@pytest.fixture(scope="class")
def CreateObject(request):
    request.cls.Calc=Calculator()

@pytest.fixture(params=[(1,2,2),(10,20,200),(100,200,20000),(2000,3000,6000000)])
def ProvidePositiveValues(request):
    return request.param

@pytest.fixture(params=[{"Num1":2,"Num2":1,"Result":1},{"Num1":80,"Num2":20,"Result":60},{"Num1":900,"Num2":50,"Result":850},{"Num1":50000,"Num2":40000,"Result":10000}])
def ProvidePositiveValuesForAddition(request):
    return request.param