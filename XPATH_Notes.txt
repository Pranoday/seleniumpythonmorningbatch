select * from Student
select * from Student where RollNo=1

Xpath is a query language
<bookstore>		Root Element Node
  <book>		Element Node			
    <title lang="en">Harry Potter</title>	Element Node
    <author>J K. Rowling</author>		Element Node
    <year>2005
       <publication year>2006</publication year>	
    </year>					Element Node
    <price>29.99</price>			Element Node
  </book>
  <bookstore>	
  </bookstore>
  <book>		Element Node			
    <title lang="Ma">Vyakti Ani Valli</title>	Element Node
    <author>P.L.Deshpande</author>		Element Node
    <year>1990</year>				Element Node
    <price>30.00</price>			Element Node
  </book>

  <book>		Element Node			
    <title lang="en">Stay Hungry Stay Foolish</title>	Element Node
    <author>Rashmi Bansal</author>		Element Node
    <year>2000</year>				Element Node
    <price>31.00</price>			Element Node
  </book>

</bookstore>

lang="en"				        Attribute Node	

Books elements are Children of Bookstore
Bookstore element is a Parent of Book elements
Book elements are sibling of each other
<title> element is preceding sibling of <author> element
<year> element is following sibling of <author> element

DIRECT RELATIONSHIPS:
	PARENT,CHILD,SIBLING

CHILD:CHILD refers to only IMMEDIATE CHILDs
DESCENDANTS:-It includes DIRECT AS WELL AS INDIRECT CHILDREN
PARENT:It includes only IMMEDIATE PARENTs
ANCESTOR:It includes EVERY NODE WHICH falls on PARENT HIERARCHY

bookstore				It will return us ALL bookstore elements from page
/bookstore				It will return ONLY ROOT ELEMENT if it has name as bookstore
//book					It will return all book elements
<bookstore>
	<book></book>
	<bookstore_1>
		<book></book>
	<bookstore_1>	
</bookstore>
bookstore/book				It will return all CHILD elements of bookstore with the name book
bookstore//book				It will return all DESCENDANT elements of bookstore with the name book
bookstore/book[1]			It will return 1st CCHILD element with the name book of bookstore				
bookstore/book[last()]			It will return LAST CHILD element with the name book of boostore
					Node returned may be different at different point of time
					Hence it is DYNAMIC XPATH
//title[@lang='Ma']  (XPATH)			This will return title element having lang attribute set to "Ma"
title[lang='Ma']     (CSS)	
bookstore/book[last()-1]		It will return SECOND LAST CHILD element with the name book of bookstore 
//author[text()='Rashmi Bansal']	It will return author element having "Rashmi Bansal" as a text in it
//author[contains(text(),'Rashmi')]	It will return author element having "Rashmi" word in the text of it

Xpath for finding checkbox related to employee named "Blue":
//table/descendant::a[text()='blue']/parent::td/preceding-sibling::td/child::input

/html/body/div/div   Absolute Xpath(which starts from Root element and continues to visit every element
until we reach required element)
//div   Relative Xpath