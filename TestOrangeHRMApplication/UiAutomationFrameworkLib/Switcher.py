from UiAutomationFrameworkLib.InvokeBrowsers import StartChromeDriver, StartFirefoxDriver, \
    StartEdgeDriver

InvokeDriver={"CHROME":StartChromeDriver,
              "FIREFOX":StartFirefoxDriver,
              "EDGE":StartEdgeDriver}