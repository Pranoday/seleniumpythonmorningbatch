'''
    LAYER 3------>
    UiAutomation lib layer
    This layer contains implementation for all methods which allows us to interact with
    different kinds of controls like EditControl,PushButton,Dropdown etc.

    This layer is MOST technical layer containing usage of Selenium APIs

    Object of this class is going to be required to each page class
'''
import csv

from selenium.webdriver.common.by import By

from UiAutomationFrameworkLib.Switcher import InvokeDriver
from UiAutomationFrameworkLib.Utilities.Utils import Utils


class WebTest:
    Driver=None
    def __init__(self):
        #ObjectRepo is a dictionary saving WebElements against the Keys
        self.ObjectRepo={}

    def StartBrowser(self,BrowserName):
        '''
            switch(BrowserName):
            {
                case "CHROME":
                    driver = webdriver.Chrome(executable_path="D:\\SeleniumPythonMorningBatch\\chromedriver.exe")
                    driver.get("https://courses.letskodeit.com/practice")
                    driver.maximize_window()
                    break;    
                case "FIREFOX":
                    driver = webdriver.Firefox(executable_path="D:\\SeleniumPythonMorningBatch\\geckodriver.exe")
                    driver.get("https://courses.letskodeit.com/practice")
                    driver.maximize_window()
            }
        '''
        Function=InvokeDriver.get(BrowserName)
        Function()
    def OpenURL(self):
        #WebTest.Driver.get("https://opensource-demo.orangehrmlive.com/")
        WebTest.Driver.get(Utils.EnvVariables.get("ApplicationURL"))

    def EnterText(self,Element,TextToEnter):
        Element.clear()
        Element.send_keys(TextToEnter)

    def ClickElement(self,Element):
        Element.click()

    def AppendText(self,Element,TextToAppend):
        Element.send_keys(TextToAppend)

    def SelectValueFromDropdown(self,Element,ValueSelectionStratergy,ValueToSelect):
        pass

    def CreateObjectRepository(self, ObjectRepositoryFileName):
        FilePath = Utils.EnvVariables.get("ObjectRepositoriesLocation") + "\\" + ObjectRepositoryFileName + "_Web" + ".csv"
        with open(FilePath) as ObjectRepo:
            csv_reader = csv.reader(ObjectRepo, delimiter=',')
            # In row we will get the content of a row as a tuple
            # Each member of tuple will represent column from that row
            for row in csv_reader:
                # This loop will read each row containing identification data with
                # every iteration
                # e.g. in 1st iteration row=(UserNameField,BY_ID,txtUsername)
                # e.g. in 2nd iteration row=(PasswordField,BY_CSS,input[type='password'])
                # Calling function FindAndReturnElement with Identification stratergy token
                # and locator as arguments.
                # Identification stratergy token is in row[1]
                # Locator is in row[2]
                #For 1st iteration=self.FindAndReturnElement("BY_ID", "txtUsername")
                El = self.FindAndReturnElement(row[1], row[2])
                self.ObjectRepo[row[0]] = El

    def FindAndReturnElement(self,StrategyToken,Locator):
        IdentificationTuple=self.CreateStratergyLocatorTuple(StrategyToken,Locator)
        RequiredElement=WebTest.Driver.find_element(*IdentificationTuple)
        return RequiredElement


    def CreateStratergyLocatorTuple(self, StrategyToken,Locator):
            StratergyLocatorTuple = {
                "BY_ID": (By.ID, Locator),
                "BY_CLASS": (By.CLASS_NAME, Locator),
                "BY_NAME": (By.NAME, Locator),
                "BY_CSS": (By.CSS_SELECTOR, Locator),
                "BY_XPATH": (By.XPATH, Locator),
                "BY_LINKTEXT": (By.LINK_TEXT, Locator)
            }
            return StratergyLocatorTuple.get(StrategyToken)

    def GetTextFromElement(self,Element):
        return Element.text