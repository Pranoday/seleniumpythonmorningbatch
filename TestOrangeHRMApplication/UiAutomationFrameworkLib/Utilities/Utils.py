import csv


class Utils:
    EnvVariables={}
    @classmethod
    def InitialiseEnvVars(self):
            with open('D:\\SeleniumPythonMorningBatch\\TestOrangeHRMApplication\\EnvVars.csv') as csv_file:
                csv_reader = csv.reader(csv_file, delimiter=',')
                for row in csv_reader:
                    Utils.EnvVariables[row[0]] = row[1]

    @classmethod
    def CreateTestData(self):

        DataList=[]
        with open('D:\\SeleniumPythonMorningBatch\\TestOrangeHRMApplication\\TestDataFiles\\LoginTestData.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                TestDataDict = {}
                TestDataDict["UserName"] = row[0]
                TestDataDict["Password"]=row[1]
                TestDataDict["ErrorMessage"]=row[2]
                DataList.append(TestDataDict)

        return DataList