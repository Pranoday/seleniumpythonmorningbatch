from selenium import webdriver

from UiAutomationFrameworkLib.Utilities.Utils import Utils


def StartChromeDriver():
    # WebTest.Driver=webdriver.Chrome(executable_path="D:\\XoriantPythonSeleniumPostmanTraining\\Drivers\\chromedriver.exe")
    from UiAutomationFrameworkLib.WebTest import WebTest
    WebTest.Driver = webdriver.Chrome(
        executable_path=Utils.EnvVariables.get("ChromeDriverPath"))

def StartFirefoxDriver():
    # WebTest.Driver=webdriver.Chrome(executable_path="D:\\XoriantPythonSeleniumPostmanTraining\\Drivers\\chromedriver.exe")
    from UiAutomationFrameworkLib.WebTest import WebTest
    WebTest.Driver = webdriver.Firefox(
        executable_path=Utils.EnvVariables.get("FirefoxDriverPath"))

def StartEdgeDriver():
    # WebTest.Driver=webdriver.Chrome(executable_path="D:\\XoriantPythonSeleniumPostmanTraining\\Drivers\\chromedriver.exe")
    from UiAutomationFrameworkLib.WebTest import WebTest
    WebTest.Driver = webdriver.edge(
        executable_path=Utils.EnvVariables.get("EdgeDriverPath"))
