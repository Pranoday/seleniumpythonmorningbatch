from UiAutomationFrameworkLib.WebTest import WebTest
'''
    Object of WebTest class is created here and kept in variable name T.
    This T variable is inherited in all page classes.

'''

class BasePage:
    def __init__(self):
        self.T=WebTest()


