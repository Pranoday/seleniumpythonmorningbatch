'''

    LAYER 2 : Page Objects

    This class implements different business processes supported by Login Page
    Every page of an application supports some or the other business processes, implementing all
    business processes from all application pages inside a single file would be difficult to
    maintain. Hence we create separate class to implement business processes from each applucation
    page.

    So LoginPage class will implement business processes from Login Page,Dashboard Page class
    will implement business processes from Dashboard page and like that.


'''
from PageObjects.BasePage import BasePage
from UiAutomationFrameworkLib.Utilities.Utils import Utils


class LoginPage(BasePage):

   def  __init__(self,BrowserName):
    super().__init__()
    Utils.InitialiseEnvVars()
    '''
            Here we are calling constructor of BasePage and making sure that
            object of WebTest gets created very first as we need it for
            all rest of the operations
    '''
    self.T.StartBrowser(BrowserName)
    self.T.OpenURL()
    self.T.CreateObjectRepository("LoginPage")

   def DoLogin(self,UserName,Password):

       '''
        Enter UserName in UserName field
        Enter Password in Password field
        Click on Login button

        Change:
            Select gender
       '''
       self.T.EnterText(self.T.ObjectRepo["UserNameField"],UserName)
       self.T.EnterText(self.T.ObjectRepo["PasswordField"],Password)
       self.T.ClickElement(self.T.ObjectRepo["LoginButton"])