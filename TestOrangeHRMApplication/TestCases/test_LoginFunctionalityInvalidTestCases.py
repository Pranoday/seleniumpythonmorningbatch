import pytest


@pytest.mark.usefixtures("CreateLoginPageObj")
class TestLoginWithInvalidData:
    loginpage=None

    def test_LoginFunctionalityWithInvalidData(self,SupplyTestDataForLogin):
        TestLoginWithInvalidData.loginpage.DoLogin(SupplyTestDataForLogin["UserName"],SupplyTestDataForLogin["Password"])
        ErrorMessageElement=TestLoginWithInvalidData.loginpage.T.FindAndReturnElement("BY_ID","spanMessage")
        ActualErrorMessage=TestLoginWithInvalidData.loginpage.T.GetTextFromElement(ErrorMessageElement)
        assert ActualErrorMessage==SupplyTestDataForLogin["ErrorMessage"],"Wrong error message is shown"