import pytest

from PageObjects.LoginPage import LoginPage
from UiAutomationFrameworkLib.Utilities.Utils import Utils


@pytest.fixture(scope="class")
def CreateLoginPageObj(request):
    loginpage = LoginPage("CHROME")
    request.cls.loginpage = loginpage


@pytest.fixture(params=Utils.CreateTestData())
def SupplyTestDataForLogin(request):
    return request.param

