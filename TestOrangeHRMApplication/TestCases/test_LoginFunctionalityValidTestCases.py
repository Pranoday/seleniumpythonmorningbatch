'''

    LAYER-------1
    Test case layer:

    This layer is going to contain test cases.

    Test case:
    1.Open browser
    2.Open application URL : https://opensource-demo.orangehrmlive.com/
    3.Enter "Admin" in UserName field
    3.Enter "admin123" in Password field
    4.Click on Login button
    5.Confirm that Dashboard page is opened
'''
import pytest

from PageObjects.LoginPage import LoginPage

@pytest.mark.usefixtures("CreateLoginPageObj")
class TestLoginWithValidCredentials:
    loginpage=None
    def test_LoginFunctionalityWithValidCredentials(self):
        '''
            driver = webdriver.Chrome(executable_path="D:\\SeleniumPythonMorningBatch\\chromedriver.exe")
            driver.get("https://courses.letskodeit.com/practice")
            driver.maximize_window()

            UserNameField=driver.find_element(By.ID,"txtUsername")
            UserNameField.send_keys("Admin")

            PasswordField=driver.find_element(By.CSSSELECTOR,"input[type='password']")
            PasswordField.send_keys("admin123")

            LoginButton=driver.find_element(By.XPATH,"//input[@value='LOGIN']")
            LoginButton.click()
        '''
        #loginpage=LoginPage("CHROME")
        TestLoginWithValidCredentials.loginpage.DoLogin("Admin","admin123")
