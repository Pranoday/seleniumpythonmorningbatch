from selenium import webdriver
from selenium.webdriver.common.by import By

driver = webdriver.Chrome(executable_path="D:\\SeleniumPythonMorningBatch\\chromedriver.exe")
driver.get("https://courses.letskodeit.com/practice")
driver.maximize_window()

NameField=driver.find_element(By.NAME,"enter-name")
'''
    get_attribute(AttributeName) returns value of an attribute for an element
'''
HintText=NameField.get_attribute("placeholder")
if(HintText=="Enter Your Name"):
    print("Correct Hint text is shown..PASSED")
else:
    print("Correct Hint text is NOT shown..FAILED")