import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select

driver = webdriver.Chrome(executable_path="D:\\SeleniumPythonMorningBatch\\chromedriver.exe")
driver.get("https://courses.letskodeit.com/practice")
driver.maximize_window()

Listbox=driver.find_element(By.ID,"multiple-select-example")
Se=Select(Listbox)
#We are selecting First value using by_index stratergy
Se.select_by_index(0)
time.sleep(3)
Se.select_by_value("orange")
time.sleep(3)

ExpectedSelectedOptions=["Apple","Orange"]
'''
    In case of Listbox we can select multiple options.And if we want to
    get all those multiple options selected then we need to use "all_selected_options" 
    all_selected_options will return list of option tags corresponding to 
    Selected options
    
    SelectedOptions=
    <option value="apple">Apple</option>
    <option value="orange">Orange</option>
'''
SelectedOptions=Se.all_selected_options
for SelectedOption in SelectedOptions:
    SelectedOptionText=SelectedOption.text
    if(SelectedOptionText in ExpectedSelectedOptions):
        print("Correct option is selected..PASSED")
    else:
        print("Correct option is NOT selected..FAILED")

ExpectedOptions=["Apple","Orange","Peach"]
AllOptions=Se.options
for Option in AllOptions:
    OptionText=Option.text
    if(OptionText in ExpectedOptions):
        print("Correct Option is present..PASSED")
    else:
        print("Correct Option is NOT present..FAILED")