from selenium import webdriver
from selenium.webdriver.common.by import By

driver = webdriver.Chrome(executable_path="D:\\SeleniumPythonMorningBatch\\chromedriver.exe")
driver.get("https://courses.letskodeit.com/practice")
driver.maximize_window()

BmwCheck=driver.find_element(By.ID,"bmwcheck")
BmwCheck.click()

IsChecked=BmwCheck.is_selected()
if(IsChecked==True):
    print("Checkbox got checked after clicking on it..PASSED")
else:
    print("Checkbox did not get checked after clicking on it..FAILED")

BmwCheck.click()

IsChecked=BmwCheck.is_selected()
if(IsChecked==False):
    print("Checkbox got Unchecked after clicking on it..PASSED")
else:
    print("Checkbox did not get Unchecked after clicking on it..FAILED")

