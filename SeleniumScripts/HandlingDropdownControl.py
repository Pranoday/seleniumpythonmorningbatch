import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select

driver = webdriver.Chrome(executable_path="D:\\SeleniumPythonMorningBatch\\chromedriver.exe")
driver.get("https://courses.letskodeit.com/practice")
driver.maximize_window()

#Default Value Testcase
'''
<select id="carselect" name="cars"> 
    <option value="bmw">BMW</option> 
    <option value="benz">Benz</option> 
    <option value="honda">Honda</option> 
</select>

'''

CarDropdown=driver.find_element(By.ID,"carselect")
'''
    To handle select type of control Selenium library has special class
    implemented called Select
    
    We have wrapped actual WebElement while creating object of Select class
    as while performing different actions using Select class object it needs to
    know which WebElement operations need to perform on.
'''
Se=Select(CarDropdown)
'''
    first_selected_option returns option tag of selected value
    e.g. If BMW is selected then it will return following:
    <option value="bmw">BMW</option>
'''
SelectedOption=Se.first_selected_option
if(SelectedOption.text=="BMW"):
    print("Correct default value is selected..PASSED")
else:
    print("InCorrect default value is selected..FAILED")

#2 Select each option and check if correct option is selected in Dropdown
'''
    We have following 3 ways to select options from Dropdown:
    1.By Index      Index of options starts from 0
    2.By Value
    3.By VisibleText
'''
Se.select_by_index(1)
time.sleep(3)
# For select_by_value we need to use value attribute
Se.select_by_value("honda")
time.sleep(3)
Se.select_by_visible_text("BMW")
time.sleep(3)

SelectedOption=Se.first_selected_option
if(SelectedOption.text=="BMW"):
    print("Correct option value is selected..PASSED")
else:
    print("InCorrect option value is selected..FAILED")
#  Check all options are correct or not
ExpectedCarOptions=["BMW","Honda","Benz"]
'''
    options property returns all option tags as List of WebElement
    i.e.
    AllOptions=<option value="bmw">BMW</option> 
    <option value="benz">Benz</option> 
    <option value="honda">Honda</option>
    
'''
AllOptions=Se.options
for option in AllOptions:
    OptionTxt=option.text
    if(OptionTxt in ExpectedCarOptions):
        print("Correct option is present in dropdown..PASSED")
    else:
        print("Correct option is NOT present in dropdown..FAILED")