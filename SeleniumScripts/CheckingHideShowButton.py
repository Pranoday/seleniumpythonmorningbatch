'''
    Script needs to start chromedriver.exe and establish a communication session
    with it.
    Then each Selenium API will be sent to a driver for translation.
    Driver will translate each API into Chrome Native UI Automation API
    and automate the Chrome browser
'''

from selenium import webdriver
from selenium.webdriver.common.by import By

driver = webdriver.Chrome(executable_path="D:\\SeleniumPythonMorningBatch\\chromedriver.exe")
driver.get("https://courses.letskodeit.com/practice")
driver.maximize_window()
#Clicking on Hide button

'''
    <input id="hide-textbox" class="btn-style class2" value="Hide" onclick="hideElement()" type="submit">
    id and name attributes are usually set to Unique values as they are also used in Client-side validations
'''

Hidebutton=driver.find_element_by_id("hide-textbox")
Hidebutton.click()

'''
    <input id="displayed-text" name="show-hide" class="inputs displayed-class" placeholder="Hide/Show Example" type="text" style="display: block;">
'''

TextBox=driver.find_element_by_name("show-hide")
'''
    is_displayed() returns boolean True if element is Visible on page else it returns boolean False
'''
IsShown=TextBox.is_displayed()
if(IsShown==False):
    print("Hide button works fine..PASSED")
else:
    print("Hide button did not work fine..FAILED")
'''
<input id="show-textbox" class="btn-style class2" value="Show" onclick="showElement()" type="submit">
'''
ShowButton=driver.find_element_by_id("show-textbox")
ShowButton.click()

IsShown=TextBox.is_displayed()
if(IsShown==True):
    print("Show button works fine..PASSED")
else:
    print("Show button did not work fine..FAILED")
