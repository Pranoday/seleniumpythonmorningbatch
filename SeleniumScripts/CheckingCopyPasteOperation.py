import time

from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select

driver = webdriver.Chrome(executable_path="D:\\SeleniumPythonMorningBatch\\chromedriver.exe")
driver.get("https://opensource-demo.orangehrmlive.com/")
driver.maximize_window()

UserNameField=driver.find_element(By.ID,"txtUsername")
UserNameField.send_keys("Pranoday")

PasswordField=driver.find_element(By.ID,"txtPassword")
Act=ActionChains(driver)
'''
    Ctrl+a is chain of following 3 events:
    1. Press Ctrl key and Hold it
    2. Type character 'a'
    3. Release Ctrl key

'''

Act.key_down(Keys.CONTROL).send_keys("a").key_up(Keys.CONTROL).perform()
time.sleep(3)
Act.key_down(Keys.CONTROL).send_keys("c").key_up(Keys.CONTROL).perform()
time.sleep(3)
Act.key_down(Keys.CONTROL,element=PasswordField).send_keys("v").key_up(Keys.CONTROL).perform()
time.sleep(3)
TypedText=PasswordField.get_attribute("value")
if(TypedText==""):
    print("Paste operation is disabled on Password field..PASSED")
else:
    print("Paste operation is NOT disabled on Password field..FAILED")
