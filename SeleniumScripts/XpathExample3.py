import time

from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select

driver = webdriver.Chrome(executable_path="D:\\SeleniumPythonMorningBatch\\chromedriver.exe")
driver.get("https://www.w3schools.com/html/tryit.asp?filename=tryhtml_table_intro")
driver.maximize_window()

driver.switch_to.frame("iframeResult")
CompanyNameElement=driver.find_element(By.XPATH,"//table/descendant::tr[last()]/td[1]")
CompanyName=CompanyNameElement.text
if(CompanyName=="Magazzini Alimentari Riuniti"):
    print("Records in table got sorted correctly..PASSED")
else:
    print("Records in table is not  sorted correctly..FAILED")