import time

from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select

driver = webdriver.Chrome(executable_path="D:\\SeleniumPythonMorningBatch\\chromedriver.exe")
driver.get("https://courses.letskodeit.com/practice")
driver.maximize_window()

PriceElement=driver.find_element(By.XPATH,"//td[text()='Python Programming Language']/following-sibling::td")
Price=PriceElement.text
if(Price=="30"):
    print("Correct price is shown..PASSED")
else:
    print("InCorrect price is shown..FAILED")
