import time

from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select

driver = webdriver.Chrome(executable_path="D:\\SeleniumPythonMorningBatch\\chromedriver.exe")
driver.get("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_table_test")
driver.maximize_window()
driver.switch_to.frame("iframeResult")
SavingsElement=driver.find_element(By.XPATH,"//table//td[text()='February']/following-sibling::td")
SavingsText=SavingsElement.text
if(SavingsText=="$80"):
    print("Correct savings are displayed..PASSED")
else:
    print("Correct savings are NOT displayed..FAILED")
