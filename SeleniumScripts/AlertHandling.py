import time

from selenium import webdriver
from selenium.webdriver.common.by import By

driver = webdriver.Chrome(executable_path="D:\\SeleniumPythonMorningBatch\\chromedriver.exe")
driver.get("https://courses.letskodeit.com/practice")
driver.maximize_window()

NameField=driver.find_element(By.ID,"name")
NameField.send_keys("Mayuri")
time.sleep(3)
'''
    Text which we enter into an EditControl/TextBox gets set to "value" attribute of it.
    So if we want to retrieve a text we typed inside Textbox then we need to use
    get_attribute("value")
'''
EnteredName=NameField.get_attribute("value")
AlertBtn=driver.find_element(By.ID,"alertbtn")
AlertBtn.click()
time.sleep(3)
'''
    driver by default works on a Browser's DOM(Document Object Model i.e. controls loadded
    as a part of web page)
    
    But browser Alert is not a part of browser DOM , hence we need to change driver's
    working context from DOM to alert
'''

Al=driver.switch_to.alert
Message=Al.text
if(EnteredName in Message):
    print("Correct alert message is shown..PASSED")
else:
    print("Correct alert message is shown..FAILED")
Al.accept()