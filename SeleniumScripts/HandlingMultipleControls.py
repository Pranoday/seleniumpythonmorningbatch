import time

from selenium import webdriver
from selenium.webdriver.common.by import By

driver = webdriver.Chrome(executable_path="D:\\SeleniumPythonMorningBatch\\chromedriver.exe")
driver.get("https://courses.letskodeit.com/practice")
driver.maximize_window()
'''
    When we want to work on single element we use find_element() and we use
    attribute which is UNIQUELY set
    
    BUT
    
    When we want to work on Multiple elements we use find_elements() and we use
    attribute which is COMMONLY set
    
    When we need to use such an attribute for which we do not have any identification
    stratergy available then we need to use By.CSS_SELECTOR
    
    SYNTAX: TAGNAME[AttributeName='AttributeValue']

    FindChild:
    
    When we use driver.find_element OR driver.find_elements, element finding is done
    on Entire page.
    
    BUT
    
    When we use Element.find_element or Element.find_elements() , element finding is
    done only under the Element we are using to call find_element
    
    In example below
     
     driver.find_elements(By.CSS_SELECTOR,"input[type='checkbox']")
     will return us 5 Checkboxes found on Entire page
     
     WhereAs
     
     DivElement.find_elements(By.CSS_SELECTOR,"input[type='checkbox']")
     
     will return us 3 checkboxes which are under div element which is stored in variable
     DivElement
     
'''
DivElement=driver.find_element(By.ID,"checkbox-example-div")
Checkboxes=DivElement.find_elements(By.CSS_SELECTOR,"input[type='checkbox']")
#Checkboxes=driver.find_elements(By.CSS_SELECTOR,"input[type='checkbox']")
for Checkbox in Checkboxes:
    Checkbox.click()
    time.sleep(3)
    if(Checkbox.is_selected()==True):
        print("Checkbox got checked after clicking on it")
    else:
        print("Checkbox did not gtt checked after clicking on it")

time.sleep(3)

for Checkbox in Checkboxes:
    Checkbox.click()
    time.sleep(3)
    if(Checkbox.is_selected()==False):
        print("Checkbox got checked after clicking on it")
    else:
        print("Checkbox did not gtt checked after clicking on it")

