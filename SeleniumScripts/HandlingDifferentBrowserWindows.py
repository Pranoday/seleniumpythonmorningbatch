import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select

driver = webdriver.Chrome(executable_path="D:\\SeleniumPythonMorningBatch\\chromedriver.exe")
driver.get("https://courses.letskodeit.com/practice")
driver.maximize_window()
'''
    When we create a Selenium script session by creating an object of driver class(chromedriver class
    in this case), driver exe opens, it in turn opens browser window and communication session
    gets created. Driver exe.
    
    When we open any software, OS sets a UNIQUE property called WndHandle to each software window like
    notepad,browser,Pycharm etc. we have opened
    
    At the time of session establishment driver retrieves the value of WndHandle property of
    browser window which our driver opens up. As this WndHandle is unique to a browser window
    opened,it helps our driver to identify which window to work on. 

    When as a part of script a new browser window than the original one opens up we need to
    retrieve WndHandle set to it and hand it over to the driver so that driver can switch its
    working context from Original Window to a New window 

    Driver always works on a browser window WndHandle of which it holds. This browser window is 
    reffered to as Current Window
    
    A browser window which gets opened at the start at the time of session establishment is called
    Original Window
'''
OpenWindowButton=driver.find_element(By.ID,"openwindow")
OpenWindowButton.click()

# This property returns handle of Original Window
CurrentWindowHandle=driver.current_window_handle
print(CurrentWindowHandle)
'''
    Following property returns List of Handles of ALL Browser windows opened in Script session.
    In short it will contain handle of Original and all New browser windows opened
'''
AllHandles=driver.window_handles
print(AllHandles)

for Handle in AllHandles:
    driver.switch_to.window(Handle)
    if(driver.title=="All Courses"):
      break;

'''
    Here once deiver will have the handle of newly opened window all APIs called with
    the reference of driver object will be performed on a window handle of which driver is 
    currently holding
'''

driver.find_element(By.LINK_TEXT,"SIGN IN").click()
time.sleep(5)

# It closes the CURRENT WINDOW
driver.close()
time.sleep(5)
#It terminates driver exe itself which results into closing all browser windows opened in
#  that script session
driver.quit()
