import time

from selenium import webdriver
from selenium.webdriver.common.by import By

driver = webdriver.Chrome(executable_path="D:\\SeleniumPythonMorningBatch\\chromedriver.exe")
driver.get("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_input_test")
driver.maximize_window()
'''
    driver by default searches for an element in MAIN html page.
    Our elements are inside NESTED html page. Nested HTML pages are loaded inside Iframe.
    
    If we want driver to search for an element inside NESTED HTML page being loaded
    inside Iframe then we need to switch driver's context to Iframe.
'''
driver.switch_to.frame("iframeResult")
FirstNameField=driver.find_element(By.ID,"fname")
FirstNameField.send_keys("Pranoday")
FirstName=FirstNameField.get_attribute("value")

LastNameField=driver.find_element(By.ID,"lname")
LastNameField.send_keys("Dingare")
LastName=LastNameField.get_attribute("value")

SubmitButton=driver.find_element(By.CSS_SELECTOR,"input[value='Submit']")
SubmitButton.click()
time.sleep(5)
'''
    To identify element using class attribute we have By.CLASSNAME.
    But if we have an element having COMPOSITE VALUE set to a Class attribute.
    COMPOSITE VALUE means multiple classes with space in between.
    
    For example:
    
    We have a div tag having "w3-container w3-large w3-border" set as a value
    to class attribute.
    
    Now this is a COMPOSITE VALUE i.e. it contains 3 values:
    w3-container
    w3-large
    w3-border
    
    Then we can not use By.CLASSNAME and we need to use BY.CSS_SELECTOR
    If we have only single value set to Class attribute then only we can use BY.CLASSNAME
'''

DivElement=driver.find_element(By.CSS_SELECTOR,"div[class='w3-container w3-large w3-border']")
DivText=DivElement.text

if(FirstName in DivText and LastName in DivText):
    print("Data submitted successfully...PASSED")
else:
    print("Data submitted successfully...FAIL")

'''
    Once driver's working context is set to frame it continues to look for
    elements inside the frame.But when we want driver to search for an element
    inside MAIN page then we need to switch driver's working context back to MAIN page
    using switch_to.default_content()
'''
driver.switch_to.default_content()
RunButton=driver.find_element(By.ID,"runbtn")
RunButton.click()