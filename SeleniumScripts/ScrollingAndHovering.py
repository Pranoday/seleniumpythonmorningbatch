import time

from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select

driver = webdriver.Chrome(executable_path="D:\\SeleniumPythonMorningBatch\\chromedriver.exe")
driver.get("https://courses.letskodeit.com/practice")
driver.maximize_window()

#Scrolling the page
Button=driver.find_element(By.ID,"mousehover")
'''By default scrollIntoView() scrolls the page until an element gets aligned to
    TOP of the page
'''
#driver.execute_script("arguments[0].scrollIntoView();",Button)

'''
    If we want to scroll the page until an element gets aligned to
    BOTTOM of the page then we need to call scrollIntoView() with false
    argument 
'''

#driver.execute_script("arguments[0].scrollIntoView(false);",Button)

driver.execute_script("arguments[0].scrollIntoView({block:\"center\"});",Button)
time.sleep(3)
'''
    To perform advanced Keyboard operations( e.g. Ctr+c) and mouse operations
    (e.g. Right Click,Drag-Drop etc.) we need to use ActionChains class
'''
Act=ActionChains(driver)
Act.move_to_element(Button).perform()
time.sleep(3)
TopLink=driver.find_element(By.CSS_SELECTOR,"a[href='#top']")
TopLink.click()

